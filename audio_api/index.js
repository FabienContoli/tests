// Import the MP3 file
//  import audioFile from './audio.mp3' assert {type: 'audio/mpeg'};

let audioContextSingleton
let mp3AudioPromise = fetch("./audio.mp3")
let audioBuffer
let buffers=[]

// Function to load an MP3 file
async function loadMP3(audioContext) {
    if (audioBuffer)
        return audioBuffer
    try {
        // Fetch the MP3 file
        const response = await mp3AudioPromise
        const arrayBuffer = await response.arrayBuffer();

        // Decode the audio data
        audioBuffer = await audioContext.decodeAudioData(arrayBuffer);

        return audioBuffer;
    } catch (error) {
        console.error('Error loading MP3:', error);
        return null;
    }
}

function addFilter(audioContext,node, type, frequency, gain) {
    const filter = audioContext.createBiquadFilter();
    filter.type = type;
    filter.gain.value = gain
    filter.frequency.value = frequency;
    node.connect(filter);
    return filter
}

function addFilters(audioContext,node) {
    const gain = -50
    let filter = addFilter(audioContext,node, "highshelf", 12000, gain)
    filter = addFilter(audioContext,filter, "lowshelf", 50, gain)
    return filter
}

function addCompressor(audioContext,node){

    const compressor = audioContext.createDynamicsCompressor();

    // Set the compression parameters
    compressor.threshold.value = -10; // Set the threshold level in decibels (dB)
    compressor.knee.value = 10; // Set the knee value in decibels (dB)
    compressor.ratio.value = 10; // Set the compression ratio
    compressor.attack.value = 0.003; // Set the attack time in seconds
    compressor.release.value = 0.25; // Set the release time in seconds
    
    node.connect(compressor)
    return compressor
}

function addGainNode(audioContext,node){

    const gainNode = audioContext.createGain();
    
    console.log(gainNode.gain.maxValue); // 3.4028234663852886e38
    node.connect(gainNode)
    return gainNode
}


// Function to play an AudioBuffer
function playAudioBuffer(audioContext, audioBuffer) {
    if (!audioBuffer) {
        console.error('AudioBuffer is null or undefined.');
        return;
    }

    // Create an AudioBufferSourceNode
    const source = audioContext.createBufferSource();

    // Set the decoded audio data to the buffer of the source node
    source.buffer = audioBuffer;

    // Create a DynamicsCompressorNode
   
    const compressor = addCompressor(audioContext,source)

    const filter = addFilters(audioContext,compressor)

    const gainNode = addGainNode(audioContext,filter)
   
    
    // Connect the source node to the destination (speakers)
    gainNode.connect(audioContext.destination);

    // Start playing the audio
    source.start();
    gainNode.gain.setValueAtTime(0.2, audioContext.currentTime);
    buffers.push(source)
}

function getAudioContext() {
    if (audioContextSingleton == null) {
        audioContextSingleton = new AudioContext();
    }
    return audioContextSingleton
}

async function getMp3Buffer(audioContext) {
    if (audioBuffer == null) {
        audioBuffer = await loadMP3(audioContext);
    }
    return audioBuffer
}

function playSound() {
    (async () => {
        const audioContext = new AudioContext();
        //const audioContext = getAudioContext()// <<--- ca craque
        const audioBufferMp3 = await getMp3Buffer(audioContext)

        if (audioBufferMp3) {
            playAudioBuffer(audioContext, audioBufferMp3);
        }
    })();
}

window.addEventListener("click", playSound)

function stopSound() {
    (async () => {
        const buffer = buffers.pop()
        if (buffer)
        buffer.stop(0)
    })();
}

window.addEventListener("wheel", stopSound)
